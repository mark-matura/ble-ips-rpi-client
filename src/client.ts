import { measurementQueue } from './helpers/MeasurementQueue';
import { serverController } from './helpers/ServerController';
import { Peripheral } from '@abandonware/noble';
import { webservice } from './helpers/Webservice';
import { consoleHelper } from './helpers/ConsoleHelper';

import BLEController from '@abandonware/noble' ;

let getDeviceTypeFromCLIArgs = function (): string {
    return process.argv.includes('gateway') ? 'gateway': 'client';
};

let deviceType: string = getDeviceTypeFromCLIArgs();

const uploadingInterval: number = parseInt(process.env.UPLOADING_INTERVAL!);

consoleHelper.printMsg();

webservice.storeClient('IPS Client');

// as soon as BLE adapter changes state
BLEController.on('stateChange', (state: any) => {
    if (state === 'poweredOn') {
        scanForDevices()

        setInterval(() => {
            if (measurementQueue.queue.length) {
                measurementQueue.uploadAndClear();
            }
        }, uploadingInterval);
    }
});

const scanForDevices = () => {
    // when CTRL + C detected, exit
    process.on('SIGINT', function() {
        process.exit();
    });

    BLEController.startScanning([process.env.SERVICE_UUID!], true);
}


BLEController.on('scanStop', () => {
    console.log('stopped scanning, restarting scan');
    scanForDevices()
});

BLEController.on('discover', (server: Peripheral) => {
    serverController.register(server);

    if (server.address && server.rssi) {
        measurementQueue.add({
            rssi: server.rssi,
            server_addr: server.address,
            origin: deviceType,
            timestamp: Date.now()
        });
    }
});