// https://dbdiagram.io/d/5db08df602e6e93440f28ebd

export interface Measurement {
    rssi: number;       // measurement in dBm
    server_addr: string;
    distance?: number;
    origin: string;
    timestamp?: number;
}