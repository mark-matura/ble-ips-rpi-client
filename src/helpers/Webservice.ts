import { Server } from '../models/server';
import { Measurement } from '../models/measurement';

import axios from 'axios';
import getMAC from 'getmac';

// get default base urls from the .env file
axios.defaults.baseURL = process.env.API_BASE_URL;

const fakeClientMac = 'dc:a6:32:3d:c9:ba';
/**
 * @brief Abstract interface for outbound HTTP connections
 *
 * @see https://www.npmjs.com/package/axios
 */
class Webservice {
    public async storeClient(name: string) {
        try {
            const response = await axios.post('/client/store', {
                name,
                // mac_addr: getMAC(),
                mac_addr: fakeClientMac,
            });

            return response.data;
        } catch (error) {
            console.error(error);
            process.exit(1);
        }
    }

    public async storeServer(server: Server) {
        try {
            const response = await axios.post('/server/store', {
                mac_addr: server.mac_addr,
            });

            return response.data;
        } catch (error) {
            console.error(error);
            process.exit(1);
        }
    }

    public async storeMeasurementQueue(measurementQueue: Measurement[]) {
        try {
            const response = await axios.post('/measurement/store', {
                // client_addr: getMAC(),
                client_addr: fakeClientMac,
                measurements: measurementQueue,
            });

            return response.data;
        } catch (error) {
            console.error(error);
            if (error.data.message && error.data.message == 'read error on connection to 127.0.0.1:6379') {
                return;
            } else {
                process.exit(1);
            }
        }
    }

    public async calibrateServer(server: Server) {
        try {
            const response = await axios.post('/server/calibrate', {
                mac_addr: server.mac_addr,
            });
            return response.data;
        } catch (error) {
            console.error(error);
            process.exit(1);
        }
    }
}

export const webservice = new Webservice();