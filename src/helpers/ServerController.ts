import { webservice } from './Webservice';
import { Peripheral } from '@abandonware/noble';

class ServerController {
    private servers: Peripheral[] = [];

    public register(server: Peripheral): void {
        if (!this.hasBeenDiscovered(server)) {
            this.servers.push(server);

            if (server.address) {
                webservice.storeServer({
                    mac_addr: server.address,
                })
            }
        }
    }

    private hasBeenDiscovered(server: Peripheral): Peripheral | undefined {
        const foundServer = this.servers.find((existingServer: Peripheral) => {
            return existingServer.address === server.address;
        })

        return foundServer
    }
}

export const serverController = new ServerController;