import { webservice } from './Webservice';
import { Measurement } from '../models/measurement';

class MeasurementQueue {
    public queue: Measurement[] = [];

    public add(measurement: Measurement) {
        console.log(measurement.rssi + "\t\t" + measurement.server_addr + "\t\t" + measurement.timestamp);

        this.queue.push(measurement);
    }

    public uploadAndClear() {
        console.log();
        webservice.storeMeasurementQueue(this.queue);
        this.queue = [];
    }
}

export const measurementQueue = new MeasurementQueue;