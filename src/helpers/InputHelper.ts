import readline from 'readline' ;

readline.emitKeypressEvents(process.stdin);
process.stdin.setRawMode(true);

class InputHelper {
    public delay(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    public waitForUserInput(query: string): Promise<number> {
        const iostream = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        return new Promise(resolve => iostream.question(query, ans => {
            let answer: number = Number(ans);
            iostream.close();
            resolve(answer);
        }))
    }

    public waitForUserToPressEnter(query: string): Promise<unknown> {
        const iostream = readline.createInterface({
            input: process.stdin,
            output: process.stdout,
            terminal: false
        });

        return new Promise(resolve => iostream.question(query, ans => {
            iostream.close();
            resolve(ans);
        }))
    }
}

export const inputHelper = new InputHelper();