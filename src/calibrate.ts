/**
 * @brief oversee calibration measurement collection and bind distance to measurements
 *
 * @see https://blog.markmarolf.com/journal/2020-05-11.html#calibration-sequence
 */

import { serverController } from './helpers/ServerController';
import { Server } from './models/server';
import { Measurement } from './models/measurement';

import { inputHelper } from './helpers/InputHelper';
import { webservice } from './helpers/Webservice';
import { measurementQueue } from './helpers/MeasurementQueue';
import { consoleHelper } from './helpers/ConsoleHelper';

import BLEController from '@abandonware/noble';
import { Peripheral } from '@abandonware/noble';

let selectedServer: Server = {} as Server;
const availableServers: Server[] = [];
let measurementBuffer: Measurement[] = [];

consoleHelper.printMsg();

// as soon as BLE adapter powers on
BLEController.on('stateChange', (state: any) => {
    if (state === 'poweredOn') {
        calibrate();
    }
});

BLEController.on('discover', (server: Peripheral) => {
    // register in API
    serverController.register(server);

    if (!alreadyDiscovered(server)) {
        availableServers.push({
            mac_addr: server.address
        })
    }

    if (server.address === selectedServer.mac_addr && server.rssi) {
        console.log(server.address + ' ' + server.rssi);

        measurementBuffer.push({
            server_addr: server.address,
            rssi: server.rssi,
            origin: 'calibration',
        });
    }
});

function alreadyDiscovered(server: Peripheral): Server | undefined {
    const alreadyFound = availableServers.find((alreadyDiscoveredServer: Server) => {
        return alreadyDiscoveredServer.mac_addr === server.address;
    });

    return alreadyFound
}

async function calibrate(): Promise<void> {
    await promptUserToSelectAServerForCalibration()
    await runCalibrationSteps()

    await webservice.storeMeasurementQueue(measurementQueue.queue);
    await webservice.calibrateServer(selectedServer);

    process.exit(0);
}

async function promptUserToSelectAServerForCalibration(): Promise<void> {
    console.log('Searching for available servers nearby ...\n');

    await scanForBLEPackets(3000);
    listAvailableServers();
    await selectAServer();
}

function listAvailableServers() {
    console.log('\nSearch resulted in ' + availableServers.length + ' servers.\n\nEnter the index of which one should be calibrated')

    availableServers.sort((a, b) => (a.mac_addr > b.mac_addr) ? 1 : -1)

    availableServers.forEach((server: Server, index: number) => {
        console.log(server.mac_addr + '\t[' + index + ']');
    });
}

async function selectAServer() {
    try {
        const index: number = await inputHelper.waitForUserInput('\n');

        if (index > availableServers.length || index < 0 || isNaN(index)) {
            throw new Error('Invalid server index');
        }

        selectedServer = availableServers[index];
        console.log("Successfully selected the server " + selectedServer.mac_addr);

    } catch (error) {
        console.log(error);
        process.exit(-1);
    }
}

async function runCalibrationSteps(): Promise<void> {
    console.log("Starting calibration sequence.\n");
    const calibrationDistances: number[] = [0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0]; // unit is meters

    for (const distance of calibrationDistances) {
        await inputHelper.waitForUserInput('Place client ' + distance + 'm from peripheral and press ENTER to continue\n');
        await scanForBLEPackets(5000)

        bindDistanceToBufferValues(distance);
    }
}

async function scanForBLEPackets(duration: number): Promise<void> {
    BLEController.startScanning([process.env.SERVICE_UUID!], true);

    await inputHelper.delay(duration);

    BLEController.stopScanning();
}

function bindDistanceToBufferValues(distance: number): void {
    measurementBuffer.forEach((measurement: Measurement) => {
        measurement.distance = distance;
    })

    measurementQueue.queue = measurementQueue.queue.concat(measurementBuffer)
    measurementBuffer = [];
}