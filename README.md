# ble-ips-rpi-client
BLE IPS client searches for BLE advertising packets with a given service UUID. It then sends the RSSI value and origin information to the ble-ips-api for processing. The amount of logic implemented in this client is kept at a minimum, the heavy lifting is done by the server. The node.js codebase can run on any hardware with the V8 javascript engine. Hence it is very easily embeddable in any microcontrollers or mobile devices and desktop devices. Generally speaking this means it can run on any Linux or MacOs device (not dependent on vendor hardware). The ble-ips-rpi-client is designed to run on very cheap hardware in an internet of things environment as a background service.

As governments across the world scramble to get their own BLE based contact tracing apps up and running, the relevance of this project could not be clearer. Yet the uses of positioning systems are not limited to global health crises. The BLE IPS client can be used in geofencing, floor plan tracking, warehouse administration, customized marketing and much more. This repository is intended to give you, the developer a chance do set up your own BLE based positioning system with as little effort as possible.

## Installation Guide on Raspbian Buster (Debian 10)

### Clone the project folder
```sh
git clone https://gitlab.com/mark-matura/ble-ips-rpi-client.git

cd ble-ips-rpi-client
```

## Time management for accurate timestamping
To accurately timestamp measurements, I recommend using an RTC to prevent drift on the Raspberry Pi (~18s / day without RTC). The Raspberry Pi can optionally be configured to sync its NTP time with a server in the LAN to reduce jitter to a few milliseconds. Otherwise you should at least set the NTP pool to one in your country of residence.

```sh
sudo timedatectl set-timezone UTC
sudo timedatectl set-local-rtc 0
sudo timedatectl set-ntp true
```

### Configure NTP
```sh
sudo nano /etc/systemd/timesyncd.conf
```

A list of NTP pools is available [here](https://www.pool.ntp.org/zone/ch). A tutorial how to create an NTP server in the LAN is available [here](https://vitux.com/how-to-install-ntp-server-and-client-on-ubuntu/). Add the following line in the file /etc/ntp.conf replacing tenderribs.cc with the hostname for your NTP server.

```
[Time]
NTP=tenderribs.cc
FallbackNTP=0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org
```

Check if everything is okay
```sh
# Force sync
sudo systemctl restart systemd-timesyncd

# enable service on startup
sudo systemctl enable systemd-timesyncd

# Make sure everything worked
timedatectl timesync-status
```

### Configure RTC
Enable I2C in raspi config
```
sudo raspi-config
```

Load ds3231 RTC kernel module on boot
```sh
sudo nano /boot/config.txt
...
dtoverlay=i2c-rtc,ds3231
...
```

#### Disable the fake hwclock

```sh
sudo apt-get -y remove fake-hwclock
sudo update-rc.d -f fake-hwclock remove
sudo systemctl disable fake-hwclock
```

Comment lines in /lib/udev/hwclock-set so they look like this
```
#if [-e/run/systemd/system];then
#exit 0
#if

#/sbin/hwclock --rtc=$dev --systz --badyear

#/sbin/hwclock --rtc=$dev --systz
```

### Set NTP and RTC Time on startup

Edit the /etc/rc.local file which executes every time the device boots up:

```sh
sudo nano /etc/rc.local
```

Paste the following in rc.local:

```
# Force timedatectl to sync time with NTP server
sudo timedatectl set-ntp true
sudo systemctl restart systemd-timesyncd.service

# set up new i2c hardware clock
echo ds1307 0x68 > /sys/class/i2c-adapter/i2c-1/new_device

# write the system time to the RTC
hwclock -w
```

### Display Time Info on bash startup

```
sudo nano .bashrc
```
enter
```
timedatectl timesync-status
timedatectl status
```

### Update Bluez (Bluetooth)
This project uses [Noble](https://github.com/abandonware/noble) to interface with the Raspberry Pi's Bluetooth adapter. Assuming you have Raspbian, go ahead and install the bluetooth libraries that Noble uses with the following command:
```sh
sudo apt-get install bluetooth bluez libbluetooth-dev libudev-dev
```

The version of BlueZ I used in this project is 5.43-2+deb9u2 for Raspbian Stretch and 5.50-1.2~deb10u1 for Raspbian Buster. Both should be fine.

### Install node.js

I have tried out node 8.11.1, 10.15.3 and 10.20.x. Not sure if it works with newer versions at the moment.

#### Raspberry Pi 3 Model B and higher:
```
curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
sudo apt install nodejs
node -v
```

#### Raspberry Pi Zero:
For the raspberry pi zero you have to [curl -o an ARM v6.l node version](https://www.thepolyglotdeveloper.com/2018/03/install-nodejs-raspberry-pi-zero-w-nodesource/) and install from source.
```sh
curl -o node-v10.15.3-linux-armv6l.tar.gz https://nodejs.org/dist/v10.15.3/node-v10.15.3-linux-armv6l.tar.gz
tar -xzf node-v10.15.3-linux-armv6l.tar.gz
sudo cp -r node-v10.15.3-linux-armv6l/* /usr/local/
```

### Project setup
Install bluetooth socket adapter and node modules with the following command:
```sh
npm install
```
If the Noble package version is 1.9.2-8, remove the semicolon at line 48 of index.d.ts file in node_modules/@abandonware/noble folder.

Copy the `.env.example` file and rename the copy to `.env`. Set your backend base url where you have the ble-ips-api set up by entering it in the .env file.

### Run client
Just run npm run build to transpile the typescript to javascript and then npm run client
```
npm run build

npm run client
```