"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const MeasurementQueue_1 = require("./helpers/MeasurementQueue");
const ServerController_1 = require("./helpers/ServerController");
const Webservice_1 = require("./helpers/Webservice");
const ConsoleHelper_1 = require("./helpers/ConsoleHelper");
const noble_1 = tslib_1.__importDefault(require("@abandonware/noble"));
let getDeviceTypeFromCLIArgs = function () {
    return process.argv.includes('gateway') ? 'gateway' : 'client';
};
let deviceType = getDeviceTypeFromCLIArgs();
const uploadingInterval = parseInt(process.env.UPLOADING_INTERVAL);
ConsoleHelper_1.consoleHelper.printMsg();
Webservice_1.webservice.storeClient('IPS Client');
// as soon as BLE adapter changes state
noble_1.default.on('stateChange', (state) => {
    if (state === 'poweredOn') {
        scanForDevices();
        setInterval(() => {
            if (MeasurementQueue_1.measurementQueue.queue.length) {
                MeasurementQueue_1.measurementQueue.uploadAndClear();
            }
        }, uploadingInterval);
    }
});
const scanForDevices = () => {
    // when CTRL + C detected, exit
    process.on('SIGINT', function () {
        process.exit();
    });
    noble_1.default.startScanning([process.env.SERVICE_UUID], true);
};
noble_1.default.on('scanStop', () => {
    console.log('stopped scanning, restarting scan');
    scanForDevices();
});
noble_1.default.on('discover', (server) => {
    ServerController_1.serverController.register(server);
    if (server.address && server.rssi) {
        MeasurementQueue_1.measurementQueue.add({
            rssi: server.rssi,
            server_addr: server.address,
            origin: deviceType,
            timestamp: Date.now()
        });
    }
});
//# sourceMappingURL=client.js.map