"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.serverController = void 0;
const Webservice_1 = require("./Webservice");
class ServerController {
    constructor() {
        this.servers = [];
    }
    register(server) {
        if (!this.hasBeenDiscovered(server)) {
            this.servers.push(server);
            if (server.address) {
                Webservice_1.webservice.storeServer({
                    mac_addr: server.address,
                });
            }
        }
    }
    hasBeenDiscovered(server) {
        const foundServer = this.servers.find((existingServer) => {
            return existingServer.address === server.address;
        });
        return foundServer;
    }
}
exports.serverController = new ServerController;
//# sourceMappingURL=ServerController.js.map