"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.inputHelper = void 0;
const tslib_1 = require("tslib");
const readline_1 = tslib_1.__importDefault(require("readline"));
readline_1.default.emitKeypressEvents(process.stdin);
process.stdin.setRawMode(true);
class InputHelper {
    delay(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    waitForUserInput(query) {
        const iostream = readline_1.default.createInterface({
            input: process.stdin,
            output: process.stdout
        });
        return new Promise(resolve => iostream.question(query, ans => {
            let answer = Number(ans);
            iostream.close();
            resolve(answer);
        }));
    }
    waitForUserToPressEnter(query) {
        const iostream = readline_1.default.createInterface({
            input: process.stdin,
            output: process.stdout,
            terminal: false
        });
        return new Promise(resolve => iostream.question(query, ans => {
            iostream.close();
            resolve(ans);
        }));
    }
}
exports.inputHelper = new InputHelper();
//# sourceMappingURL=InputHelper.js.map