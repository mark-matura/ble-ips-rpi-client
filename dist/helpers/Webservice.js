"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.webservice = void 0;
const tslib_1 = require("tslib");
const axios_1 = tslib_1.__importDefault(require("axios"));
// get default base urls from the .env file
axios_1.default.defaults.baseURL = process.env.API_BASE_URL;
const fakeClientMac = 'dc:a6:32:3d:c9:ba';
/**
 * @brief Abstract interface for outbound HTTP connections
 *
 * @see https://www.npmjs.com/package/axios
 */
class Webservice {
    async storeClient(name) {
        try {
            const response = await axios_1.default.post('/client/store', {
                name,
                // mac_addr: getMAC(),
                mac_addr: fakeClientMac,
            });
            return response.data;
        }
        catch (error) {
            console.error(error);
            process.exit(1);
        }
    }
    async storeServer(server) {
        try {
            const response = await axios_1.default.post('/server/store', {
                mac_addr: server.mac_addr,
            });
            return response.data;
        }
        catch (error) {
            console.error(error);
            process.exit(1);
        }
    }
    async storeMeasurementQueue(measurementQueue) {
        try {
            const response = await axios_1.default.post('/measurement/store', {
                // client_addr: getMAC(),
                client_addr: fakeClientMac,
                measurements: measurementQueue,
            });
            return response.data;
        }
        catch (error) {
            console.error(error);
            if (error.data.message && error.data.message == 'read error on connection to 127.0.0.1:6379') {
                return;
            }
            else {
                process.exit(1);
            }
        }
    }
    async calibrateServer(server) {
        try {
            const response = await axios_1.default.post('/server/calibrate', {
                mac_addr: server.mac_addr,
            });
            return response.data;
        }
        catch (error) {
            console.error(error);
            process.exit(1);
        }
    }
}
exports.webservice = new Webservice();
//# sourceMappingURL=Webservice.js.map