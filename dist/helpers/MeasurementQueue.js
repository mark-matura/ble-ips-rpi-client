"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.measurementQueue = void 0;
const Webservice_1 = require("./Webservice");
class MeasurementQueue {
    constructor() {
        this.queue = [];
    }
    add(measurement) {
        console.log(measurement.rssi + "\t\t" + measurement.server_addr + "\t\t" + measurement.timestamp);
        this.queue.push(measurement);
    }
    uploadAndClear() {
        console.log();
        Webservice_1.webservice.storeMeasurementQueue(this.queue);
        this.queue = [];
    }
}
exports.measurementQueue = new MeasurementQueue;
//# sourceMappingURL=MeasurementQueue.js.map