"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.consoleHelper = void 0;
class ConsoleHelper {
    printMsg() {
        const msg = `
        ██╗██████╗ ███████╗     ██████╗██╗     ██╗███████╗███╗   ██╗████████╗
        ██║██╔══██╗██╔════╝    ██╔════╝██║     ██║██╔════╝████╗  ██║╚══██╔══╝
        ██║██████╔╝███████╗    ██║     ██║     ██║█████╗  ██╔██╗ ██║   ██║
        ██║██╔═══╝ ╚════██║    ██║     ██║     ██║██╔══╝  ██║╚██╗██║   ██║
        ██║██║     ███████║    ╚██████╗███████╗██║███████╗██║ ╚████║   ██║
        ╚═╝╚═╝     ╚══════╝     ╚═════╝╚══════╝╚═╝╚══════╝╚═╝  ╚═══╝   ╚═╝
        `;
        console.log(msg);
    }
}
exports.consoleHelper = new ConsoleHelper();
//# sourceMappingURL=ConsoleHelper.js.map