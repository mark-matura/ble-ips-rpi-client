"use strict";
/**
 * @brief oversee calibration measurement collection and bind distance to measurements
 *
 * @see https://blog.markmarolf.com/journal/2020-05-11.html#calibration-sequence
 */
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const ServerController_1 = require("./helpers/ServerController");
const InputHelper_1 = require("./helpers/InputHelper");
const Webservice_1 = require("./helpers/Webservice");
const MeasurementQueue_1 = require("./helpers/MeasurementQueue");
const ConsoleHelper_1 = require("./helpers/ConsoleHelper");
const noble_1 = tslib_1.__importDefault(require("@abandonware/noble"));
let selectedServer = {};
const availableServers = [];
let measurementBuffer = [];
ConsoleHelper_1.consoleHelper.printMsg();
// as soon as BLE adapter powers on
noble_1.default.on('stateChange', (state) => {
    if (state === 'poweredOn') {
        calibrate();
    }
});
noble_1.default.on('discover', (server) => {
    // register in API
    ServerController_1.serverController.register(server);
    if (!alreadyDiscovered(server)) {
        availableServers.push({
            mac_addr: server.address
        });
    }
    if (server.address === selectedServer.mac_addr && server.rssi) {
        console.log(server.address + ' ' + server.rssi);
        measurementBuffer.push({
            server_addr: server.address,
            rssi: server.rssi,
            origin: 'calibration',
        });
    }
});
function alreadyDiscovered(server) {
    const alreadyFound = availableServers.find((alreadyDiscoveredServer) => {
        return alreadyDiscoveredServer.mac_addr === server.address;
    });
    return alreadyFound;
}
async function calibrate() {
    await promptUserToSelectAServerForCalibration();
    await runCalibrationSteps();
    await Webservice_1.webservice.storeMeasurementQueue(MeasurementQueue_1.measurementQueue.queue);
    await Webservice_1.webservice.calibrateServer(selectedServer);
    process.exit(0);
}
async function promptUserToSelectAServerForCalibration() {
    console.log('Searching for available servers nearby ...\n');
    await scanForBLEPackets(3000);
    listAvailableServers();
    await selectAServer();
}
function listAvailableServers() {
    console.log('\nSearch resulted in ' + availableServers.length + ' servers.\n\nEnter the index of which one should be calibrated');
    availableServers.sort((a, b) => (a.mac_addr > b.mac_addr) ? 1 : -1);
    availableServers.forEach((server, index) => {
        console.log(server.mac_addr + '\t[' + index + ']');
    });
}
async function selectAServer() {
    try {
        const index = await InputHelper_1.inputHelper.waitForUserInput('\n');
        if (index > availableServers.length || index < 0 || isNaN(index)) {
            throw new Error('Invalid server index');
        }
        selectedServer = availableServers[index];
        console.log("Successfully selected the server " + selectedServer.mac_addr);
    }
    catch (error) {
        console.log(error);
        process.exit(-1);
    }
}
async function runCalibrationSteps() {
    console.log("Starting calibration sequence.\n");
    const calibrationDistances = [0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0]; // unit is meters
    for (const distance of calibrationDistances) {
        await InputHelper_1.inputHelper.waitForUserInput('Place client ' + distance + 'm from peripheral and press ENTER to continue\n');
        await scanForBLEPackets(5000);
        bindDistanceToBufferValues(distance);
    }
}
async function scanForBLEPackets(duration) {
    noble_1.default.startScanning([process.env.SERVICE_UUID], true);
    await InputHelper_1.inputHelper.delay(duration);
    noble_1.default.stopScanning();
}
function bindDistanceToBufferValues(distance) {
    measurementBuffer.forEach((measurement) => {
        measurement.distance = distance;
    });
    MeasurementQueue_1.measurementQueue.queue = MeasurementQueue_1.measurementQueue.queue.concat(measurementBuffer);
    measurementBuffer = [];
}
//# sourceMappingURL=calibrate.js.map